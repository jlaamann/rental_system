package rental_system;

import org.joda.time.DateTime;
import org.junit.Test;
import static org.junit.Assert.*;
import rental_system.models.FilmModel;
import rental_system.models.InventoryModel;
import rental_system.models.RentalModel;

public class RentalSystemTest {

    private FilmModel testOldFilm = new FilmModel("La Dolce Vita", "OLD");
    private FilmModel testRegFilm = new FilmModel("Die Hard 2", "REGULAR");
    private FilmModel testNewFilm = new FilmModel("Star Wars VIII", "NEW");

    @Test
    public final void testRentalModel_GetPrice_2Days() {
        // + 2 days
        RentalModel rmOld = new RentalModel(testOldFilm, 2);
        RentalModel rmReg = new RentalModel(testRegFilm, 2);
        RentalModel rmNew = new RentalModel(testNewFilm, 2);
        assertEquals(rmOld.getPrice(), 3);
        assertEquals(rmReg.getPrice(), 3);
        assertEquals(rmNew.getPrice(), 8);
    }

    @Test
    public final void testRentalModel_GetPrice_3Days() {
        // + 3 days
        RentalModel rmOld = new RentalModel(testOldFilm, 3);
        RentalModel rmReg = new RentalModel(testRegFilm, 3);
        RentalModel rmNew = new RentalModel(testNewFilm, 3);
        assertEquals(rmOld.getPrice(), 3);
        assertEquals(rmReg.getPrice(), 3);
        assertEquals(rmNew.getPrice(), 12);
    }


    @Test
    public final void testRentalModel_GetPrice_5Days() {
        // + 5 days
        RentalModel rmOld = new RentalModel(testOldFilm, 5);
        RentalModel rmReg = new RentalModel(testRegFilm, 5);
        RentalModel rmNew = new RentalModel(testNewFilm, 5);
        assertEquals(rmOld.getPrice(), 3);
        assertEquals(rmReg.getPrice(), 9);
        assertEquals(rmNew.getPrice(), 20);
    }


    @Test
    public final void testRentalModel_GetPrice_7Days() {
        // + 7 days
        RentalModel rmOld = new RentalModel(testOldFilm, 7);
        RentalModel rmReg = new RentalModel(testRegFilm, 7);
        RentalModel rmNew = new RentalModel(testNewFilm, 7);
        assertEquals(rmOld.getPrice(), 9);
        assertEquals(rmReg.getPrice(), 15);
        assertEquals(rmNew.getPrice(), 28);
    }

    @Test
    public final void testInventoryModel_getStock() {
        InventoryModel im = new InventoryModel();
        im.addFilm(testNewFilm);
        im.addFilm(testRegFilm);
        im.addFilm(testOldFilm);
        im.addRental(testOldFilm);
        assertEquals(im.getStock().size(), 2);
        assertTrue(im.getStock().contains(testNewFilm));
        assertTrue(im.getStock().contains(testRegFilm));
        assertFalse(im.getStock().contains(testOldFilm));
    }

    @Test
    public final void testInventoryModel_removeRental() {
        InventoryModel im = new InventoryModel();
        im.addFilm(testNewFilm);
        im.addFilm(testRegFilm);
        im.addFilm(testOldFilm);
        im.addRental(testOldFilm);
        im.removeRental("La Dolce Vita");
        assertEquals(im.getStock().size(), 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public final void testFilmModel_setType_throws_IllegalArgumentException() {
        FilmModel im = new FilmModel("Vertigo", "NEW");
        im.setType("abcdefg");
    }
}
