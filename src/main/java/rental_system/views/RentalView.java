package rental_system.views;

import java.util.List;

public class RentalView {
    public void displayTransaction(List<String> rentals, int totalPrice) {
        System.out.println("====== NEW RENTAL ======");
        for (String rentalInfo : rentals) {
            System.out.println(rentalInfo);
        }
        System.out.println("Total price : " + String.valueOf(totalPrice) + " EUR");
    }

    public void displayLateRental(List<String> rentals, int totalCharge) {
        System.out.println("====== LATE RENTAL ======");
        for (String rental : rentals) {
            System.out.println(rental);
        }
        System.out.println("Total charge : " + totalCharge + " EUR");
    }

    public void displayStock(List<String> films) {
        System.out.println("====== STOCK ======");
        for (String film : films) {
            System.out.println(film);
        }
    }

    public void displayAllFilms(List<String> films) {
        System.out.println("====== ALL FILMS ======");
        for (String film : films) {
            System.out.println(film);
        }
    }

    public void displayRemainingBonusPoints(int bonusPoints) {
        System.out.println("Remaining bonus points: " + bonusPoints);
    }
}
