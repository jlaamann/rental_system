package rental_system.controllers;

import org.joda.time.DateTime;
import org.joda.time.Days;
import rental_system.models.CustomerModel;
import rental_system.models.FilmModel;
import rental_system.models.InventoryModel;
import rental_system.models.RentalModel;
import rental_system.views.RentalView;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RentalSystemController {

    private InventoryModel inventory;
    private RentalView view;

    private DateTime dateTime; // for simulation purposes

    public RentalSystemController() {
        this.inventory = new InventoryModel();
        this.view = new RentalView();
    }

    public RentalSystemController(DateTime dateTime) {
        this.inventory = new InventoryModel();
        this.view = new RentalView();
        this.dateTime = dateTime;
    }

    public RentalSystemController(InventoryModel inventory, DateTime dateTime) {
        this.inventory = inventory;
        this.dateTime = dateTime;
    }

    /**
     * Returns a new CustomerModel with selected name
     * @param name
     * @return
     */
    public CustomerModel createCustomer(String name) {
        return new CustomerModel(name);
    }

    /**
     * Adds new film to the inventory.
     * @param title
     * @param type
     */
    public void addFilm(String title, String type) {
        FilmModel film = new FilmModel(title, type);
        inventory.addFilm(film);
    }

    /**
     * Removes a film from the inventory
     * @param title
     */
    public void removeFilm(String title) {
        inventory.removeFilm(title);
    }

    /**
     * Updates film's type in the inventory
     * @param title
     * @param type
     */
    public void updateFilm(String title, String type) {
        inventory.updateFilm(title, type);
    }

    /**
     * Registers a single new rental.
     * @param customer
     * @param title
     * @param days
     * @param startDate
     */
    public void newRental(CustomerModel customer, String title, int days, DateTime startDate) {
        List<Map.Entry<String, Integer>> titles = new ArrayList<>();
        titles.add(new AbstractMap.SimpleEntry<String, Integer>(title, days));
        newRentals(customer, titles, startDate);
    }

    /**
     * Registers a singele new rental without having to specify the start date.
     * @param customer
     * @param title
     * @param days
     */
    public void newRental(CustomerModel customer, String title, int days) {
        newRental(customer, title, days, new DateTime());
    }

    /**
     * Registers multiple new rentals.
     * @param customer
     * @param titles Titles are contained within List<Map.Entry<String, Integer>> as title - rental duration pairs
     * @param startDate
     */
    public void newRentals(CustomerModel customer, List<Map.Entry<String, Integer>> titles, DateTime startDate) {
        List<String> transaction = new ArrayList<>();
        int totalPrice = 0;
        boolean bonusPointsUsed = false;
        for (Map.Entry<String, Integer> title : titles) {
            FilmModel film = inventory.getFilm(title.getKey());
            int days = title.getValue();
            if (film != null) {
                RentalModel rental = new RentalModel(film, startDate, days);
                customer.addRental(rental);
                inventory.addRental(rental.getFilm());
                // use bonus points to pay if possible
                if (film.getType().equals("NEW") && customer.getBonusPoints() >= days * RentalModel.BONUS_POINTS_PER_DAY) {
                    transaction.add(rental.getBonusRentalInfo(days * RentalModel.BONUS_POINTS_PER_DAY));
                    customer.removeBonusPoints(days * RentalModel.BONUS_POINTS_PER_DAY);
                    bonusPointsUsed = true;
                } else {
                    totalPrice += rental.getPrice();
                    transaction.add(rental.getRentalInfo());
                }
                int bonusPoints = 1;
                if (film.getType().equals("NEW"))
                    bonusPoints = 2;
                customer.addBonusPoints(bonusPoints);
            } else {
                throw new IllegalArgumentException("No such film!");
            }
        }
        view.displayTransaction(transaction, totalPrice);
        if (bonusPointsUsed)
            view.displayRemainingBonusPoints(customer.getBonusPoints());
    }

    /**
     * Returns rentals and shows charge for late rentals.
     * @param customer
     * @param rentals
     * @param currentDate
     */
    public void returnRentals(CustomerModel customer, List<RentalModel> rentals, DateTime currentDate) {
        List<String> lateRentals = new ArrayList<>();
        List<String> titles = new ArrayList<>();
        int totalLateCharge = 0;
        for (RentalModel rental : rentals) {
            titles.add(rental.getFilm().getTitle());
            if (rental.isLate(currentDate)) {
                totalLateCharge += rental.getLateChargePrice(currentDate);
                lateRentals.add(rental.getLateRentalInfo(currentDate));
            }
        }
        // removes the returned films from the customers rentals' list
        for (String title : titles) {
            customer.removeRental(title);
        }
        if (!lateRentals.isEmpty())
            view.displayLateRental(lateRentals, totalLateCharge);
    }

    /**
     * Returns single rental.
     * @param customer
     * @param rental
     */
    public void returnRental(CustomerModel customer, RentalModel rental) {
        List<RentalModel> rentals = new ArrayList<>();
        rentals.add(rental);
        returnRentals(customer, rentals, new DateTime());
    }

    /**
     * Returns rentals without having to specify date (current system date is used instead).
     * @param customer
     * @param rentals
     */
    public void returnRentals(CustomerModel customer, List<RentalModel> rentals) {
        returnRentals(customer, rentals, new DateTime());
    }

    /**
     * Shows all films in the inventory.
     */
    public void showAllFilms() {
        List<String> filmTitles = new ArrayList<>();
        for (FilmModel fm : inventory.getInventory()) {
            filmTitles.add(fm.getInfo());
        }
        view.displayAllFilms(filmTitles);
    }

    /**
     * Shows all the films in the inventory that are not rented out.
     */
    public void showStock() {
        List<String> filmTitles = new ArrayList<>();
        for (FilmModel fm : inventory.getStock()) {
            filmTitles.add(fm.getInfo());
        }
        view.displayStock(filmTitles);
    }

    // FOR SIMULATION

    /**
     * Jumps given amount of days ahead.
     * @param days
     */
    public void simulate(int days) {
        this.dateTime = dateTime.plusDays(days);
    }

    public DateTime getDateTime() {
        return dateTime;
    }
}
