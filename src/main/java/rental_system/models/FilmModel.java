package rental_system.models;

public class FilmModel {

    private String title;
    private String type;

    public FilmModel(String title, String type) {
        this.title = title;
        setType(type);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public String getTypeVerbose() {
        String result = "";
        if (this.type.equals("NEW"))
            result =  "New release";
        else if (this.type.equals("REGULAR"))
            result = "Regular rental";
        else if (this.type.equals("OLD"))
            result = "Old film";
        return result;
    }

    /**
     * Returns a string in the following format: "<TITLE>, <TYPE>"
     * @return
     */
    public String getInfo() {
        return this.title + ", " + this.type;
    }

    /**
     * Sets type for the film
     * @param type
     */
    public void setType(String type) {
        if (type.equals("NEW") || type.equals("REGULAR") || type.equals("OLD")) {
            this.type = type;
        } else {
            throw new IllegalArgumentException("Type must be either NEW, REGULAR or OLD");
        }
    }
}
