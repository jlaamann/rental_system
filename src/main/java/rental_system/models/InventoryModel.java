package rental_system.models;

import java.util.ArrayList;
import java.util.List;

public class InventoryModel {
    private List<FilmModel> inventory;
    private List<FilmModel> currentlyRented;

    public InventoryModel(List<FilmModel> inventory) {
        this.inventory = inventory;
        this.currentlyRented = new ArrayList<>();
    }

    public InventoryModel() {
        this.inventory = new ArrayList<>();
        this.currentlyRented = new ArrayList<>();
    }

    /**
     * Adds a new film to inventory
     * @param film
     */
    public void addFilm(FilmModel film) {
        inventory.add(film);
    }

    /**
     * Returns film based on its title
     * @param title
     * @return
     */
    public FilmModel getFilm(String title) {
        for (FilmModel film : getStock()) {
            if (film.getTitle().equals(title))
                return film;
        }
        return null;
    }

    /**
     * Updates the list currentlyRented when a new rental is registered
     * @param film
     */
    public void addRental(FilmModel film) {
        currentlyRented.add(film);
    }

    /**
     * Removes film based on its title.
     * @param title
     */
    public void removeFilm(String title) {
        inventory.removeIf((FilmModel f) -> f.getTitle().equals(title));
        currentlyRented.removeIf((FilmModel f) -> f.getTitle().equals(title));
    }

    /**
     * Removes rental from the list currentlyRented.
     * @param title
     */
    public void removeRental(String title) {
        currentlyRented.removeIf((FilmModel f) -> f.getTitle().equals(title));
    }

    /**
     * Updates film's type based on its title.
     * @param title
     * @param newType
     */
    public void updateFilm(String title, String newType) {
        for (FilmModel film : inventory) {
            if (film.getTitle().equals(title))
                film.setType(newType);
        }
    }

    /**
     * Returns all the films that are registered in the system.
     * @return
     */
    public List<FilmModel> getInventory() {
        return inventory;
    }

    /**
     * Returns films that are not rented out.
     * @return
     */
    public List<FilmModel> getStock() {
        List<FilmModel> stock = new ArrayList<>(inventory.size());
        stock.addAll(inventory);
        stock.removeAll(currentlyRented);
        return stock;
    }
}
