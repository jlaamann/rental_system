package rental_system.models;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.util.Date;


public class RentalModel {

    public static final int PREMIUM_PRICE = 4;
    public static final int BASIC_PRICE = 3;
    public static final int BONUS_POINTS_PER_DAY = 25;

    private FilmModel film;
    private DateTime startDate;
    private DateTime dueDate;
    private int days;

    public RentalModel(FilmModel film, DateTime startDate, int days) {
        this.film = film;
        this.startDate = startDate;
        this.dueDate = startDate.plusDays(days);
        this.days = days;
    }

    public RentalModel(FilmModel film, int days) {
        this.film = film;
        this.startDate = new DateTime();
        this.dueDate = startDate.plusDays(days);
        this.days = days;
    }

    public FilmModel getFilm() {
        return film;
    }

    /**
     * Calculates the price paid upfront based on the film type and
     * the duration of rental
     * @return price
     */
    public int getPrice() {
        int price = 0;
        if (film.getType().equals("NEW")) {
            price = PREMIUM_PRICE * days;
        } else if (film.getType().equals("REGULAR")) {
            if (this.days <= 3) {
                price = BASIC_PRICE;
            } else {
                price = BASIC_PRICE + BASIC_PRICE * (this.days - 3);
            }
        } else if (film.getType().equals("OLD")) {
            if (this.days <= 5) {
                price = BASIC_PRICE;
            } else {
                price = BASIC_PRICE + BASIC_PRICE * (this.days - 5);
            }
        }
        return price;
    }

    /**
     * Calculates the price for the late charge when the date of the return
     * exceeds due date
     * @param endDate the return date of the rental
     * @return price for the rental
     */
    public int getLateChargePrice(DateTime endDate) {
        Days d = Days.daysBetween(dueDate, endDate);
        int daysLate = d.getDays();

        int price = 0;

        if (film.getType().equals("NEW")) {
            price = PREMIUM_PRICE * daysLate;
        } else if (film.getType().equals("REGULAR")) {
            price = BASIC_PRICE * daysLate;
        } else if (film.getType().equals("OLD")) {
            price = BASIC_PRICE * daysLate;
        }

        return price;
    }

    /**
     * Calculates the rental price based on real system date
     * @return  price for the rental
     */
    public int getLateChargePrice() {
        return getLateChargePrice(new DateTime());
    }

    public void setFilm(FilmModel film) {
        this.film = film;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    /**
     * Chekcs if rental has exceeded due date
     * @param currentDate
     * @return true if is late, false otherwise
     */
    public boolean isLate(DateTime currentDate) {
        return getDaysLate(currentDate) > 0;
    }

    /**
     * Returns the number of days that has passed since the due date
     * @param currentDate
     * @return
     */
    public int getDaysLate(DateTime currentDate) {
        Days d = Days.daysBetween(dueDate, currentDate);
        return d.getDays();
    }

    /**
     * Returns string in following format: "<Title> (<Type>) n days n EUR"
     * @return
     */
    public String getRentalInfo() {
        return film.getTitle() + " (" + film.getTypeVerbose() + ") " + String.valueOf(this.days)
                + " days " + String.valueOf(this.getPrice()) + " EUR";
    }

    /**
     * Returns string in following format: "<Title> (<Type>) n days (Paid with n bonus points)"
     * @return
     */
    public String getBonusRentalInfo(int bonusPoints) {
        return film.getTitle() + " (" + film.getTypeVerbose() + ") " + String.valueOf(this.days)
                + " days (Paid with " + String.valueOf(bonusPoints) + " bonus points)";
    }

    /**
     * Returns string in following format: "<Title> (<Type>) n extra days n EUR"
     * @param currentDate
     * @return
     */
    public String getLateRentalInfo(DateTime currentDate) {
        return film.getTitle() + " (" + film.getTypeVerbose() + ") " + String.valueOf(this.getDaysLate(currentDate))
                + " extra days " + String.valueOf(this.getLateChargePrice(currentDate)) + " EUR";
    }
}
