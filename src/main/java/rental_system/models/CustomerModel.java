package rental_system.models;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class CustomerModel {

    private String name;
    private int bonusPoints;
    private List<RentalModel> rentals;

    public CustomerModel(String name) {
        this.name = name;
        this.bonusPoints = 0;
        this.rentals = new ArrayList<>();
    }

    /**
     * Adds bonus points to the user account
     * @param amount number of bonus points
     */
    public void addBonusPoints(int amount) {
        if (amount > 0) {
            this.bonusPoints += amount;
        } else {
            throw new IllegalArgumentException("Bonus points must be greater than 0");
        }
    }

    public int getBonusPoints() {
        return bonusPoints;
    }

    /**
     * Removes specified amount of bonus points from the user account.
     * @param amount
     */
    public void removeBonusPoints(int amount) {
        if (amount <= this.bonusPoints)
            this.bonusPoints -= amount;
        else
            throw new IllegalArgumentException("Bonus points can't be negative");
    }

    public void addRental(RentalModel rental) {
        rentals.add(rental);
    }

    /**
     * Removes a rented film based on its title
     * @param title title of the rented film
     */
    public void removeRental(String title) {
        rentals.removeIf((RentalModel r) -> r.getFilm().getTitle().equals(title));
    }

    public List<RentalModel> getRentals() {
        return rentals;
    }

}
