package rental_system;

import org.joda.time.DateTime;
import rental_system.controllers.RentalSystemController;
import rental_system.models.CustomerModel;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Application {
    public static void main(String[] args) {
        DateTime currentDate = new DateTime(2018, 3, 13, 12, 00);

        RentalSystemController rentalSystem = new RentalSystemController(currentDate);

        System.out.println(currentDate);

        // let's add some films
        rentalSystem.addFilm("The Graduate", "OLD");
        rentalSystem.addFilm("Lady Bird", "NEW");
        rentalSystem.addFilm("Groundhog Day", "REGULAR");
        rentalSystem.addFilm("Good Time", "NEW");
        rentalSystem.addFilm("Slacker", "REGULAR");
        rentalSystem.addFilm("L'Avventura", "OLD");

        // creates a new user
        CustomerModel customer = rentalSystem.createCustomer("Testkasutaja");

        // rents a film and returns it 2 days after due date
        rentalSystem.newRental(customer, "L'Avventura", 5, currentDate);
        rentalSystem.showStock();
        rentalSystem.showAllFilms();
        rentalSystem.simulate(7);
        rentalSystem.returnRentals(customer, customer.getRentals(), rentalSystem.getDateTime());

        // remove film from inventory
        rentalSystem.removeFilm("Good Time");
        rentalSystem.showAllFilms();

        // add film to inventory & change type
        rentalSystem.addFilm("Frances Ha", "REGULAR");
        rentalSystem.updateFilm("Groundhog Day", "OLD");
        rentalSystem.showStock();

        // in order to rent multiple films at the same time, we have to create a
        // List<Map.Entry<String, Integer>> that holds film title and rental length pairs
        List<Map.Entry<String, Integer>> films = new ArrayList<>();
        films.add(new AbstractMap.SimpleEntry<String, Integer>("Frances Ha", 7));
        films.add(new AbstractMap.SimpleEntry<String, Integer>("The Graduate", 10));
        rentalSystem.newRentals(customer, films, rentalSystem.getDateTime());
        rentalSystem.simulate(10);
        rentalSystem.returnRentals(customer, customer.getRentals(), rentalSystem.getDateTime());

        // transfer 25 bonus points to the user account and use them to pay for a new rental
        customer.addBonusPoints(25);
        rentalSystem.newRental(customer,"Lady Bird", 1);
    }
}
